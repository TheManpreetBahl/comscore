"""
Manpreet Bahl
Coding Challenge Section 2.1: SELECT,ORDER,FILTER functions
Programming Language: Python 3.5.2

This file contains the implementation of the query tool
to query the datastore file created by importer.py. It
currently supports the following functions:
    1) SELECT
    2) ORDER
    3) FILTER

Assumptions made:
    1) Results of query fit in memory.
    2) Only one filter parameter is provided in query.
    3) The datastore file alreay exists.
"""
#Imports
import sys
import os
import csv
import argparse
from datetime import datetime
from collections import OrderedDict

class Query_Tool:
    """
    This class implements the query tool to query the data stored
    in the datastore file created by the importer tool.
    """
    def __init__(self, datastore_filepath):
        """
        This method initializes the class and sets the filepath
        of the datastore file.
        Parameters:
            1) datastore_filepath = the absolute filepath of the
                                    datastore file.
        Returns:
            1) N/A
        """
        #Check to make sure that there's a filepath provided
        if datastore_filepath:
            self.datastore_filepath = datastore_filepath
        else:
            raise TypeError("Datastore filepath cannot be None!")
    
    def query(self, select=None, order=None, q_filter=None):
        """
        This method reads the datastore file and collects records that
        match the query parameters provided.
        Parameters:
            1) select = comma separated list of the columns to return in
                        the query results.
            2) order = comma seperated list of the columns to sort the query
                       results by
            3) q_filter = string of the format "<column_name>=<value>" to filter
                          the query search by. Currently supports only one filter.
        Returns:
            1) List of OrderedDictionaries if there's specific select parameters,
               otherwise, list of normal Python dictionaries containing the query results
        """
        results = []
        #Open the datastore file
        with open(self.datastore_filepath, 'r') as ds_file:
            csv_reader = csv.DictReader(ds_file, fieldnames=['STB','TITLE','PROVIDER', 'DATE', 'REV', 'VIEW_TIME'])
            
            #Get the filter column and value if there's a filter provided
            if q_filter:
                parsed_filters = q_filter.split("=")
            
            #Get the columns to include in the result if there's selected columns provided
            if select:
                parsed_selects = select.split(',')

            try:
                #Read the datastore file line by line
                for data in csv_reader:
                    #Select certain columns 
                    if select:
                        #Use ordered dictionary since it preserves order by insertion.
                        #This is used for displaying the results of the query in the
                        #specified column order provided by the parameter
                        toAdd = OrderedDict()
                        #For each selected column, add it to the ordered dictionary
                        for column in parsed_selects:
                            toAdd[column] = data[column]
                        
                        #If there's a filter, then only add the ordered dictionary 
                        #if it matches the filter
                        if q_filter:
                            if data[parsed_filters[0]] == parsed_filters[1]:
                                results.append(toAdd)
                        else:
                            #No filter specified so add the record
                            results.append(toAdd)
                    else:
                        #No select modifiers so select all columns
                        #If there's a filter, then filter the data
                        if q_filter:
                            if data[parsed_filters[0]] == parsed_filters[1]:
                                results.append(data)
                        else:
                            #No filter specified so add the record
                            results.append(data)
            except Exception as e:
                raise Exception("An error was encountered when trying to query the datastore. The full error message is: " + str(e))
        
        #Order the data if provided
        if order:
            try:
                #Get the columns to order by
                parsed_orders = order.split(',')
                #Utilize Python's built-in sort, which can sort on multiple columns. 
                #Included a special case to convert datetime strings to datetime objects to ensure proper comparisions
                results = sorted(results, key=lambda x: tuple(datetime.strptime(x[c], '%Y-%m-%d') if c == "DATE" else x[c] for c in parsed_orders))
            except Exception as e:
                raise Exception("An error was encountered when trying to order the results of the query. The full error message is: " + str(e))

        return results

def main():
    """
    This method implements the command line argument tool for interacting with the datastore. 
    Parameters:
        1) N/A
    Returns:
        1) N/A
    """
    #Set up command line argument parser with accepted arguments
    parser = argparse.ArgumentParser(description='Query the datastore using SQL-like syntax.')
    parser.add_argument('--datastore', metavar='-d', help='absolute file path of the datastore')
    parser.add_argument('-s', help='Columns to select to show in results from query. Enter multiple columns as a comma separated list')
    parser.add_argument('-o', help='Order the results by specified columns. Enter multiple columns as a comma separted list')
    parser.add_argument('-f', help="Filter the query results on a particular column value")
    
    #Parse the arguments provided by the user
    args = parser.parse_args()

    #Ensure that the arguments are actually provided
    if args.datastore is None:
        args.datastore = "datastore.csv"
    
    #Initialize the query tool and run the query given the parameters provided
    tool = Query_Tool(args.datastore)
    try:
        results = tool.query(select=args.s, order=args.o, q_filter=args.f)
        
        #Print the results to standard output
        for record in results:
            print(",".join(record.values()))
    except Exception as e:
        print(str(e))
        sys.exit(1)
    
if __name__ == '__main__':
    main()

"""
Manpreet Bahl
Coding Challenge Section 1: Importer and Datastore
Programming Language: Python 3.5.2
Command Line Usage:
    python3 importer.py -h
    This will bring up the usage guide for the importer tool.

This file contains the implementation of the creating and updating a datastore 
with input files being the source of the data. The datastore stores the data in
CSV format with a maximum number of records allowed being 100 records. The records
in the datastore are unique by STB, TITLE, and DATE columns. 

Assumptions made:
    1) Datastore has maximum size. I used 100 records to simulate a maximum size reached.
    2) All input files have the same format with the headers being the first row of 
       the file. The format of the input file is correct.
"""

#Imports
import os
import csv
from datetime import datetime
import argparse

class Datastore:
    """
    This class implements the datastore used for storing records from
    file imports.
    """
    def __init__(self, datastore_filepath, max_records=100):
        """
        This method initializes the Datastore class.
        Parameters:
            1) datastore_filepath: the absolute filepath where to store
                                   the datastore file.
            2) max_records: the maximum number of records that the datastore
                            can store. the default is 100 records
        Returns:
            1) N/A
        """
        #Check to make sure that there's a filepath provided
        if datastore_filepath:
            self.datastore_filepath = datastore_filepath
            self.max_records = max_records
        else:
            raise TypeError("Datastore filepath cannot be None!")

    def __add(self, data, headers):
        """
        This method adds a record to the datastore.
        Parameters:
            1) data = a Python dictionary with the appropriate key, value
                      pairs to add to the datastore.
            2) headers = list of the dictionary's keys to use as headers for
                         reading and writing to the datastore
        Returns:
            1) N/A
        """
        #Check to see if the datastore file does not exist
        created = False
        if os.path.isfile(self.datastore_filepath) == False:
            try:
                #Create the datastore file since it doesn't exist using writing mode
                ds_file = open(self.datastore_filepath, mode ='w+')
                created = True
            except Exception:
                raise IOError("Could not open the file at: " + str(self.datastore_filepath))
        else:
            try:
                #Datastore file exists so open it for reading and writing
                ds_file = open(self.datastore_filepath, mode ='r+')
            except Exception:
                raise IOError("Could not open the file at: " + str(self.datastore_filepath))
        
        #Write to file with no checking since the datastore was just created
        if created:
            csv_writer = csv.DictWriter(ds_file, fieldnames=headers)
            csv_writer.writerow(data)
        else: 
            #Datastore file exists. Make new output file in case of any replacements
            with open(os.path.dirname(self.datastore_filepath) + 'new_datastore.csv', mode='w+') as new_ds_file:
                csv_reader = csv.DictReader(ds_file, fieldnames=headers)
                csv_writer = csv.DictWriter(new_ds_file, fieldnames=headers)
                replaced = False
                line_nums = 0
                #Copy data from old datastore file to new datastore file.
                for line in csv_reader:
                    #If data to add already exists in the datastore file, update the line before copying it to the new datastore file
                    if line['STB'] == data['STB'] and line['TITLE'] == data['TITLE'] and line['DATE'] == data['DATE']:
                        line['PROVIDER'] = data['PROVIDER']
                        line['REV'] = data['REV']
                        line['VIEW_TIME'] = data['VIEW_TIME']
                        replaced = True
                    
                    #Make sure there's enough space in the datastore file before writing to it
                    if line_nums <= self.max_records:
                        csv_writer.writerow(line)
                        line_nums += 1
                    else:
                        raise MemoryError("Unable to store any more records as datastore is full!")
                
                #No data was replaced while copying over, so add it to the new datastore file if there's any room
                if replaced == False:
                    if line_nums <= self.max_records:
                        csv_writer.writerow(data)
                    else:
                        raise MemoryError("Unable to store any more records as datastore is full!")
            
            #Delete the outdated datastore file
            os.remove(self.datastore_filepath)

            #Rename the new datastore file as the default file
            os.rename(os.path.dirname(self.datastore_filepath) + 'new_datastore.csv', self.datastore_filepath)

    def import_file(self, input_filepath):
        """
        This method parses and imports the input file and 
        adds it to the datastore, updating any records that have the
        same STB, TITLE, and DATE columns.
        Parameters:
            1) input_filepath = the absolute filepath of the file to
                                import to the datastore
        Returns:
            1) N/A
        """
        try:
            #Make sure that input filepath is not None
            if input_filepath:
                #Parse the input file
                with open(input_filepath) as input_file:
                    #Strip out the headers of the file
                    headers = input_file.readline().strip('\n').split('|')

                    #Parse the data in the remainder of the file
                    for line in input_file:
                        #Add each line to a dictionary
                        entry = {}
                        parsed_line = line.strip('\n').split('|')
                        for key,value in enumerate(parsed_line):
                            entry[headers[key]] = value
                        
                        #Check to make sure each field is valid given the constraints
                        if len(entry['STB']) > 64:
                            raise ValueError("STB cannot be longer than 64 characters!")
                        elif len(entry['TITLE']) > 64:
                            raise ValueError("TITLE cannot be longer than 64 characters!")
                        elif len(entry['PROVIDER']) > 64:
                            raise ValueError("PROVIDER cannot be longer than 64 characters!")

                        try:
                            datetime.strptime(entry['DATE'], '%Y-%m-%d')
                        except ValueError:
                            raise ValueError("Invalid format for DATE! Format expected is: YYYY-MM-DD")

                        try:
                            datetime.strptime(entry['VIEW_TIME'], '%I:%M')
                        except ValueError:
                            raise ValueError("Invalid format for VIEW_TIME! Format expected is: hours:minutes")
                        
                        #Add the entry to the datastore
                        self.__add(entry, headers)
            else:
                raise TypeError("Input filepath cannot be None!")
        except (ValueError, MemoryError, IOError):
            raise
        except Exception as e:
            raise RuntimeError("Encountered an error when importing file. The full error message is: " + str(e))
    
def main():
    """
    This method implements the command line argument tool for
    interacting with the datastore. 
    Parameters:
        1) N/A
    Returns:
        1) N/A
    """
    #Set up command line argument parser with accepted arguments
    parser = argparse.ArgumentParser(description='Import file to the datastore...')
    parser.add_argument('--datastore', metavar='-d', help='absolute file path of the datastore')
    parser.add_argument('i', help='absolute file path to import')
    
    #Parse the arguments provided by the user
    args = parser.parse_args()

    try:
        #Ensure that the arguments are actually provided
        if args.datastore is None:
            args.datastore = "datastore.csv"
        elif args.i is None:
            raise ValueError("Please proivde the absolute filepath of the file to import to the datastore!")
        
        #Initialize the datastore
        ds = Datastore(args.datastore)

        #Import the data from the file provided by the user
        ds.import_file(args.i)
    except Exception as e:
        print(str(e))

if __name__ == '__main__':
    main()